# IE Cleanser
A [Flink](https://flink.apache.org/) application used to clean the data coming from the Information Extraction task and merged with the [ie-merger](https://gitlab.unige.ch/addmin/ie-merger) script.

## How-To
### Run
The application is released as JAR that can be found in the [releases page](https://gitlab.unige.ch/addmin/ie-cleanser/-/releases). In order to run the application you must download the JAR and deploy it on a Flink cluster.

##### Default configuration
The application uses Kafka streams to read/write data, thus a kafka instance is required.

In particular, the kafka instance must:
* be accessible at the address `kafka:9092`
* have a topic called `rml-streamer-in` (from which it reads the data)

Currently, there is no manner to change this configuration. The feature will be added in the future releases.

