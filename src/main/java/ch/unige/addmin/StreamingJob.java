/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.unige.addmin;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.scala.typeutils.Types;
import org.apache.flink.connector.base.DeliveryGuarantee;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.HashMap;
import java.util.Map;

public class StreamingJob {

    private final static String KAFKA_ADDRESS = "kafka:9092";
    private final static String TOPIC_CONSUMER = "rml-streamer-in";

    public static void main(String[] args) throws Exception {
        // set up the streaming execution environment
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        KafkaSource<String> consumer = KafkaSource.<String>builder()
                .setBootstrapServers(KAFKA_ADDRESS)
                .setTopics(TOPIC_CONSUMER)
                .setStartingOffsets(OffsetsInitializer.earliest())
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();

        DataStream<String> input = env.fromSource(consumer, WatermarkStrategy.noWatermarks(), "Kafka Source")
                .map(value -> {
                    Gson gson = new Gson();
                    JsonObject jsonObj = gson.fromJson(value, JsonElement.class).getAsJsonObject();
                    JsonArray extractedFields = jsonObj.get("extractedFields").getAsJsonArray();
                    Map<String, String> mapValues = new HashMap<>();
                    for (JsonElement obj : extractedFields) {
                        String type = obj.getAsJsonObject().get("type").getAsString();
                        String val = obj.getAsJsonObject().get("value").getAsString();
                        mapValues.put(type, val);
                    }
                    mapValues.put("class", jsonObj.get("class").getAsString());
                    return gson.toJson(mapValues);
                })
                .returns(Types.STRING());

        KafkaSink<String> producer = KafkaSink.<String>builder()
                .setBootstrapServers(KAFKA_ADDRESS)
                .setDeliverGuarantee(DeliveryGuarantee.AT_LEAST_ONCE)
                .setRecordSerializer(KafkaRecordSerializationSchema.builder()
                        .setTopicSelector(element -> {
                            Gson gson = new Gson();
                            JsonObject jsonObj = gson.fromJson(String.valueOf(element), JsonElement.class).getAsJsonObject();
                            return jsonObj.get("class").getAsString();
                        })
                        .setValueSerializationSchema(new SimpleStringSchema())
                        .build()
                )
                .build();
        input.sinkTo(producer);

        // execute program
        env.execute("Information Extraction Cleanser");
    }

}
